# Certificates
The `update_cert.sh` script is used to automate the issuing of new certificates
for the TasLUG domains and uploading the new certificates to GitLab.

## Usage
The script relies on the follow environment variables being set:

  - `CF_Email`
  - `CF_Key`
  - `GITLAB_TOKEN`

The script can then simply be run using:

  ```
  certs/update_certs.sh
  ```

## Environment Variables
### `CF_Email`
The email address used to log into CloudFlare

### `CF_Key`
The `Global API Key` that can be found on the user's CloudFlare `My Profile`
page.

### `GITLAB_TOKEN`
A [Personal Access Token](https://gitlab.com/profile/personal_access_tokens)
created for the purposes of uploading the new certificates to GitLab. The `api`
scope should be ticked when creating the Personal Access Token.
