Title: Launceston Meeting: 25th August - Using Git for backup, version control and collaboration
Date: 2018-08-17 14:37 +1000
Category: Launceston

Hi people! For this month's Launceston meeting, we will be taking a look at the
F/OSS version control system, Git, going over version control basics and some
handy tips for common tasks. In addition to Git itself, we'll also be touching
on GUI front-ends and repository management suites like GitLab.

Where:
: Enterprize<br />
  22-24 Patterson St

When:
: Saturday 25th August<br />
  2:00pm Start

As always, everybody is welcome to come along and join the discussion. Hope to
see you there!
