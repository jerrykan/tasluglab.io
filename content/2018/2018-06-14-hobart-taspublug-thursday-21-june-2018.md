Title: Hobart TasPubLUG: Thursday 21 June 2018
Date: 2018-06-14
Category: Hobart
Tags: taspublug

This month we will again be meeting up and the (now traditional) Hope and
Anchor Tavern for a bite to eat and a chat about all things related to free and
open source software, hardware and culture.

When:
: 6:30pm, Thursday 21 June 2018

Where:
: Hope and Anchor Tavern (65 Macquarie St, Hobart)

If you are planning to come along (even for a quick drink) please let John know
by Tuesday so we can make the appropriate booking.

We hope to see a big turn out next Thursday.
