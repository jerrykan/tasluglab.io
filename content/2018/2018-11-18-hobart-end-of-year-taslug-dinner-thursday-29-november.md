Title: Hobart end of year TasLUG dinner: Thursday 29 November
Date: 2018-11-22
Category: Hobart

The end of the year is fast approaching and before everyone starts winding down for the holiday season there is one last opportunity for the Hobart TasLUG community to catch up for an end of year dinner.

When:
: 6:30pm, Thursday 29 November

Where:
: Amigo Mexican Restaurant<br />
  [http://amigostas.com.au/menu/](http://amigostas.com.au/menu/)

If you plan on coming along, then please **RSVP to John by Tuesday 27
November** so he can make the appropriate booking.

If Amigos are unable to fit us in, the backup option will be Mother India just down the road.

