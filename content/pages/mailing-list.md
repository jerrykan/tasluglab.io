Title: Mailing List
SortOrder: 40

The TasLUG mailing list is the primary means of announcing upcoming TasLUG
events and activities. The list is also for members to have general discussions
related to Free and Open Source software, hardware, and culture.

Members can **subscribe to the mailing list**
[here](http://lists.linux.org.au/mailman/listinfo/taslug).

All members are welcome to post to the mailing list to ask questions or
highlight topics of interest.

The mailing list archives are publicly available
[here](http://lists.linux.org.au/pipermail/taslug/).

---

## Thanks
TasLUG members are very grateful to [Linux Australia](https://linux.org.au) for
hosting the mailing list for TasLUG.
