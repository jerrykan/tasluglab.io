Title: Launceston June Meeting - An introduction to Matplotlib and NumPy
Date: 2019-06-21
Category: Launceston

Hi people. For this month's Launceston meeting, we will be taking a look at using 2D plotting library [Matplotlib](https://matplotlib.org/) and array computing package [NumPy](https://numpy.org/) to process data and create charts and graphs.

Where:
: Enterprize<br />
  22-24 Patterson St

When:
: Saturday 29th June<br />
  2:00pm Start

As always, everybody is welcome to come along and join the discussion. Hope to see you there!
