Title: Hobart TasPubLUG: Thursday 21 March - Hope and Anchor Tavern
Date: 2019-03-16
Category: Hobart
Tags: taspublug

This month sees the return of TasPubLUG in Hobart and a chance to talk about
all things related to free and open source software, hardware, and culture.

When:
: 6:30pm, Thursday 21 March 2019

Where:
: Hope and Anchor Tavern (65 Macquarie St, Hobart)

Let [John](mailto:jkristen@theintraweb.net) know if you are planning to come
along. If we hear from more than a handful of people that they are coming then
we'll book a table, otherwise we will just rock up see what is available.

Looking forward to seeing everyone next week on Thursday.
