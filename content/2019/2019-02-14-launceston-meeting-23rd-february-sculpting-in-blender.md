Title: Launceston Feburary Meeting - Sculpting in Blender
Date: 2019-02-14
Category: Launceston

Hi people! For this month's Launceston meeting, we'll be taking a look at the sculpting tools included with the Free/Open Source 3D creation suite [Blender](http://blender.org). We'll also be doing a live demonstration, creating a character (or as much of one as we can squeeze into the meeting) from scratch.

Where:
: Enterprize<br />
  22-24 Patterson St

When:
: Saturday 23rd February<br />
  2:00pm Start

This meeting's presentation may be focused toward newcomers, but as always, everybody is welcome to come along and join the discussion. Hope to see you there!
