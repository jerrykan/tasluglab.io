Title: Hobart TasPubLUG: Thursday 12 September - Watermans Hotel
Date: 2019-09-05
Author: John Kristensen
Category: Hobart

September is TasPubLUG time where we head to the pub to talk about Free and
Open Source software, hardware and culture... or just generally hang out and
have something to eat and drink.

When:
: 6:30pm, Thursday 12 September 2019

Where:
: Watermans Hotel (27 Salamanca Place, Hobart)<br/>
  (the old Quarry Salamanca)

If you are planning to come along (even for a quick drink) it would be great if
you can let [John](mailto:jkristen@theintraweb.net) know. If a largish number
of people plan to come along then we'll reserve a table.
