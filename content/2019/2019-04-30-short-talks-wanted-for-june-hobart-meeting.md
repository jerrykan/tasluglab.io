Title: Short talks wanted for June Hobart meeting
Date: 2019-04-30
Author: John Kristensen
Category: Hobart

The June meeting is still a while off yet, but we thought we would try
something a little bit different and have a bunch of short talks instead of the
usual one or two longer talks.

Some suggestions for things people might want to talk about:

  - some project they are currently working on
  - a useful tool/application/distro they think others should know about
  - some useful tips/tricks
  - a general rant about some topic

Pretty much any topic semi-related to open source software, hardware, or
culture would be welcome. If you have a topic you are a bit unsure about, feel
free to run the idea by John... he'll most likely say yes.

If you want to give a short talk, then please drop
[John](mailto:jkristen@theintraweb.net) a line letting him know the topic and
how long you think your talk will be. Ideally we would like to keep talks
between 2-10 minutes, but if you need more time let John know and we'll see
what we can schedule in.

We look forward to seeing what people come up with.
